const { Router } = require('../utils/i18nextUtils')

const routerUtils = {
  push: (router, ...args) => {
    window.scrollTo(0, 0)

    if (typeof router === 'string') {
      Router.push(router, ...args)
      return
    }

    Router.push(router.href, router.as, ...args)
  },

  replace: (router, ...args) => {
    if (typeof router === 'string') {
      return Router.replace(router, ...args)
    }

    return Router.replace(router.href, router.as, ...args)
  },

  redirect: (context, target) => {
    const location = typeof target === 'string'
      ? target
      : (target.as || target.href || '/')

    if (context.res) {
      context.res.writeHead(303, { Location: location })
      context.res.end()
      return
    }

    // In the browser, we just pretend like this never even happened
    routerUtils.replace(target)
  }
}

module.exports = routerUtils
