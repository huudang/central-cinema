import { Button } from "components/antd";
import Clearfix from "components/clearfix";
import Link from "next/link";
import PropTypes from "prop-types";
import { routerLink } from "../../../../router";
import styled from "styled-components";

const S4Header = styled.div`
  display: flex;
  justify-content: center;
  width: 100;
`;

const S4HeadingWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  flex: 1;
`;

const Header = ({ title, isShowBack = true }) => {
  return (
    <S4Header>
      {isShowBack && (
        <Button
          shape="circle"
          // icon={<ChevronLeft />}
          size="small"
          onClick={() => window.history.back()}
        />
      )}

      <S4HeadingWrapper>
        <Link {...routerLink.home.index.get().linkProps()}>
          <img
            src="/images/logo.png"
            height={248}
            width="auto"
            style={{ cursor: "pointer" }}
          />
        </Link>
      </S4HeadingWrapper>

      {isShowBack && <Clearfix width={24} height={16} />}
    </S4Header>
  );
};

Header.propTypes = {
  isShowBack: PropTypes.bool,
  title: PropTypes.string,
};

export default Header;
