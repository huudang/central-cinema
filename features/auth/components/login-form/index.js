import * as yup from "yup";

import { FastField, Form, Formik } from "formik";
import { Modal, message } from "antd";

import { Button } from "components/antd";
import Clearfix from "components/clearfix";
import Header from "../header";
import InputField from "../../../../components/formik-custom-field/input-field";
import PropTypes from "prop-types";
import React from "react";
import { useEffect, useState } from "react";
import { inject, observer } from "mobx-react";
import { routerUtils } from "../../../../router";

const LoginForm = ({ onSubmit, authStore }) => {
  const handleSubmit = async (values, formikHelpers) => {
    // if (onSubmit) {
    //   await onSubmit({ ...values }, formikHelpers);
    //   authStore.authSuccess({
    //     profle: { username: values.username, phone: values.phone },
    //   });
    // }
    authStore.authSuccess({
      profile: { username: values.username, phone: values.phone },
    });
    showModal();
  };

  useEffect(() => {
    console.log("Effect");
    authStore.authSuccess({ profle: {} });
  }, []);

  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const success = () => {
    message.success("Fill Information Success");
  };

  return (
    <>
      <Formik
        initialValues={{ username: "", phone: "" }}
        onSubmit={handleSubmit}
        validationSchema={yup.object().shape({
          username: yup
            .string()
            .required("Please input your email or username"),
          phone: yup.string().required("Please input your phone"),
        })}
      >
        {({ isSubmitting }) => {
          return (
            <Form style={{ margin: "32px" }}>
              <Header isShowBack />
              <Clearfix />
              <FastField
                name="username"
                component={InputField}
                placeholder="Join Due"
                autoFocus
                style={{
                  fontFamily: "Roboto sans-serif",
                  color: "#333",
                  margin: "0 auto",
                  borderRadius: "8px",
                  backgroundColor: "rgb(255, 255, 255)",
                  width: "90%",
                  display: "block",
                  border: 0,
                  transition: "all 0.3s",
                }}
              />

              <Clearfix h={32} />

              <FastField
                name="phone"
                component={InputField}
                type="phone"
                placeholder="Phone"
                style={{
                  fontFamily: "Roboto sans-serif",
                  color: "#333",
                  margin: "0 auto",
                  borderRadius: "8px",
                  backgroundColor: "rgb(255, 255, 255)",
                  width: "90%",
                  display: "block",
                  border: 0,
                  transition: "all 0.3s",
                }}
              />

              <Clearfix h={32} />
              <Button
                htmlType="submit"
                // type='primary'
                color="#e5e5e5"
                block
                loading={isSubmitting}
                style={{
                  font: "inherit",
                  height: "50%",
                  width: "50%",
                  borderRadius: "5em",
                  boxSizing: "border-box",
                  display: "block",
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
              >
                BOOK NOW
              </Button>
            </Form>
          );
        }}
      </Formik>
      <Modal
        title="Confirm Information"
        visible={isModalVisible}
        onOk={() => {
          success();
          routerUtils.redirect({}, "/");
        }}
        onCancel={handleCancel}
      >
        <p>You agree to this information</p>
      </Modal>
    </>
  );
};

LoginForm.propTypes = {
  onSubmit: PropTypes.func,
};

export default inject("authStore")(observer(LoginForm));
