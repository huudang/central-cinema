import { inject, observer } from 'mobx-react'
import { routerLink, routerUtils } from 'router'

import CheckAccountForm from 'features/auth/components/check-account-form'
import React from 'react'
import accountAPI from 'api/accountAPI'
import { useTranslation } from 'utils/i18nextUtils'

const CheckAccountContainer = ({ authStore }) => {
  const { t: tGlobal } = useTranslation('global')

  const handleSubmit = async (values, formikHelpers) => {
    const { countryCode, phone } = values

    authStore.setCountryCode(countryCode)
    authStore.setPhone(phone)

    const request = {
      country_code: countryCode,
      phone
    }

    console.log('POST /customeraccounts/check', request)
    const checkRes = await accountAPI.customer.check(request)
    console.log('Response : POST /customeraccounts/check', checkRes.data)

    if (!checkRes.success) {
      console.error({
        status: 'error',
        title: 'Lỗi kiểm tra tài khoản tồn tại',
        description: checkRes?.data?.error.map?.(e => tGlobal(`errorMessage.${e}`)).join?.(', '),
        isClosable: true
      })
      return
    }

    const isAccountExists = checkRes.data.id !== 0

    // login
    if (isAccountExists) {
      console.log('Start : Login')
      routerUtils.push(routerLink.auth.login.get().linkProps())
      return
    }

    // register
    console.log('Start : Register')
    routerUtils.push(routerLink.auth.registerOTP.get().linkProps())
  }

  return (
    <CheckAccountForm onSubmit={handleSubmit} />
  )
}

export default inject('authStore')(observer(CheckAccountContainer))
