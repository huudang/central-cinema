import { inject, observer } from 'mobx-react'

import OTPForm from 'features/auth/components/otp-form'
import { routerLink, routerUtils } from 'router'

const RegisterOTPContainer = ({ authStore }) => {
  const handleSubmit = (res) => {
    /* eslint-disable-next-line camelcase */
    const { id, group, auth_code, country_code, phone } = res

    authStore.setOTPId(id)
    authStore.setOTPGroup(group)
    authStore.setOTPAuthCode(auth_code)
    authStore.setCountryCode(country_code)
    authStore.setPhone(phone)

    routerUtils.push(routerLink.auth.registerInfo.get().linkProps())
  }

  return (
    <OTPForm
      countryCode={authStore.countryCode}
      phone={authStore.phone}
      otpGroup='otp_register'
      onSubmit={handleSubmit}
    />
  )
}

export default inject('authStore')(observer(RegisterOTPContainer))
