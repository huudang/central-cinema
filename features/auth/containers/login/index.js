import { inject, observer } from 'mobx-react'
import { routerLink, routerUtils } from 'router'
import { saveId, saveToken } from 'utils/auth'

import LoginForm from 'features/auth/components/login-form'
import accountAPI from 'api/authApi'
import { useTranslation } from 'utils/i18nextUtils'

const LoginContainer = ({ authStore }) => {
  const { t: tGlobal } = useTranslation('global')

  const handleSubmit = async (values, formikHelpers) => {
    const loginBody = {
      identifier: values.username,
      password: values.password
    }

    const loginRes = await accountAPI.login(loginBody)

    if (!loginRes.success) {
      const error = loginRes.data?.error.map?.(e => tGlobal(`errorMessage.${e}`)).join?.(', ')
      return
    }

    const { jwt } = loginRes.data
    const {id} = loginRes.data.user

    saveId(id)
    saveToken(jwt)
    authStore.authSuccess({ 
      profile: loginRes.data.user 
    })

    console.log(authStore)

    routerUtils.push(routerLink.home.index.get().linkProps())
  }

  return (
    <LoginForm
      onSubmit={handleSubmit}
    />
  )
}

export default inject('authStore')(observer(LoginContainer))
