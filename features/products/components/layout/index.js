import React from "react";
import Clearfix from "../../../../components/clearfix";
import MainLayout from "../../../../components/layouts";
import { Layout } from "antd";

const { Content } = Layout;

const ProductLayout = ({ children }) => {
  return (
    <MainLayout title="Products" currentTab="2">
      {/* <PageHeader title='Products' subTitle='All' /> */}
      <Clearfix height={16} />
      <Content style={{ minHeight: "500px", marginLeft: 55, marginRight: 16 }}>
        {children}
      </Content>
    </MainLayout>
  );
};

export default ProductLayout;
