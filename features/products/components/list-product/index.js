import React from 'react'
import { Row, Col, Space, Skeleton } from 'antd'
import CardProduct from '../card-product/index'

import InfiniteScroll from 'react-infinite-scroller'

const defaultColProps = {
  span: 24,
  md: 12,
  lg: 8
}

const ListProduct = ({ data = [], fetchMoreData, loading }) => {
  return (
    <InfiniteScroll
      initialLoad={false}
      loadMore={fetchMoreData}
      hasMore
      loader={(
        <Space style={{ margin: '50px' }}>
          <Skeleton.Button active={loading} size='small' shape='circle' />
          <Skeleton.Button active={loading} size='small' shape='circle' />
          <Skeleton.Avatar active={loading} size='small' shape='circle' />
          <Skeleton.Input style={{ width: 200 }} active={loading} size='small' />
        </Space>)}
    >
      <Row gutter={[16, 16]}>
        {data.map((postItem, index) => (
          <Col
            key={postItem._id}
            {...defaultColProps}
          >
            <CardProduct
              className='fadeIn animated'
              {...postItem}
            />
          </Col>
        ))}
      </Row>
    </InfiniteScroll>
  )
}

export default ListProduct
