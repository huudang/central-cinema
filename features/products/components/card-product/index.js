import React, { useState } from 'react'
import { Card } from 'antd'
import { ShoppingOutlined, ContainerOutlined } from '@ant-design/icons'

import { inject, observer } from 'mobx-react'

import DrawerProduct from '../../../../components/drawer-info-product'

const { Meta } = Card

const CardProduct = ({ title, description, authStore }) => {
  const [visible, setVisible] = useState(false)

  const onClose = () => {
    setVisible(false)
  }

  return (
    <>
      <Card
        hoverable
        style={{ width: 320 }}
        cover={<img alt='example' src='https://miro.medium.com/max/1400/1*8y1nxhqjRrNJwzYGe7JENQ.jpeg' />}
        actions={[
          <ContainerOutlined key='detail' onClick={() => setVisible(true)} />,
          <ShoppingOutlined
            key='add' onClick={() => authStore.addToCart()}
          />
        ]}
      >
        <Meta title={title} description={description} />
      </Card>
      <DrawerProduct visible={visible} onClose={onClose} />
    </>
  )
}

export default inject('authStore')(observer(CardProduct))
