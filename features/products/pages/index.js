import React, { useState, useEffect } from 'react'
import ProductLayout from '../components/layout'
import ProductList from '../components/list-product'

const initialData = [
  {
    title: 'The iPhone X is the Beginning of the End for Phones',
    description: 'Des'
  }, {
    title: 'The iPhone X is the Beginning of the End for Phones',
    description: 'Des'
  }, {
    title: 'The iPhone X is the Beginning of the End for Phones',
    description: 'Des'
  }, {
    title: 'The iPhone X is the Beginning of the End for Phones',
    description: 'Des'
  }, {
    title: 'The iPhone X is the Beginning of the End for Phones',
    description: 'Des'
  }, {
    title: 'The iPhone X is the Beginning of the End for Phones',
    description: 'Des'
  }
]

const ProductPage = () => {
  const [data, setData] = useState(initialData)
  const [loading, setLoading] = useState(false)

  const fetchMoreData = () => {
    setLoading(true)
    const newData = [...data, ...initialData]
    setTimeout(() => {
      setData(newData)
      setLoading(false)
    }, 2000)
  }

  return (
    <ProductLayout>
      <ProductList fetchMoreData={fetchMoreData} data={data} loading={loading} />
    </ProductLayout>
  )
}

export default ProductPage
