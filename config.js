import env from './env'

export const LOG_MOBX_STORE = false

export const getMainApi = () => {
  const API = env.default.API_URL
  const c = (path = '') => API + path
  return {
    auth: c('/admin/auth'),
    login: c('/auth/login'),
    register: c('/auth/register'),
    user: c('/users')
  }
}
