import React from 'react'
import { routerUtils } from 'router'
import { saveRedirectPath } from 'utils/auth'

const withIsAuthenticated = (Component) => {
  const IsAuthenticated = (props) => {
    if (!props.isAuthenticated) return null

    return <Component {...props} />
  }

  IsAuthenticated.getInitialProps = async (ctx) => {
    const { stores } = ctx
    const isAuthenticated = stores.authStore.isAuthenticated

    if (!isAuthenticated) {
      if (process.browser) {
        saveRedirectPath(null, ctx.router.asPath)
        routerUtils.redirect({}, '/auth')
      } else {
        saveRedirectPath(ctx.res, ctx.router.asPath)
        routerUtils.redirect(ctx, '/auth')
      }

      return {
        namespacesRequired: [],
        isAuthenticated
      }
    }

    const prevInitialProps = Component.getInitialProps
      ? await Component.getInitialProps(ctx)
      : {}

    return {
      ...prevInitialProps,
      isAuthenticated
    }
  }

  return IsAuthenticated
}

export default withIsAuthenticated
