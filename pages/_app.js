import * as yup from 'yup'

import { appWithTranslation, i18n } from 'utils/i18nextUtils'
import { autorun, toJS } from 'mobx'
import { getId, getToken, saveToken, getAmount } from '../utils/auth'
import { setGlobalAppLocale, setGlobalAuthorization } from '../api/axiosClient'

import App from 'next/app'
import Error from 'next/error'
import { GridThemeProvider } from 'styled-bootstrap-grid'
import Head from 'next/head'
import { LOG_MOBX_STORE } from 'config'
import NProgress from 'nprogress'
import { Provider } from 'mobx-react'
import React from 'react'
import ReactGA from 'react-ga'
import Router from 'next/router'
import accountAPI from '../api/userApi'
import env from 'env'
import { initializeStores } from 'stores'
import { v4 as uuid } from 'uuid'
import validator from 'validator'

import 'theme/scss/global.scss'
import 'theme/scss/nprogress.scss'
import 'theme/scss/cropper.scss'
import 'mobx-react-lite/batchingForReactDom'

yup.addMethod(yup.string, 'phone', function (message) {
  return this.test('phone', message, (value) => {
    if (!value) return true
    return validator.isMobilePhone(value, 'vi-VN')
  })
})

export const logPageView = () => {
  console.log(`Logging pageview for ${window.location.pathname}`)
  ReactGA.set({ page: window.location.pathname })
  ReactGA.pageview(window.location.pathname)
}

Router.onRouteChangeStart = () => {
  NProgress.start()
}

Router.onRouteChangeComplete = () => {
  if (process.env.NODE_ENV === 'production') {
    logPageView()
  }
  NProgress.done()
}

Router.onRouteChangeError = () => NProgress.done()

const gridTheme = {
  row: {
    padding: 16
  },
  col: {
    padding: 16
  },
  container: {
    padding: 32
  }
}

class MyApp extends App {
  static async getInitialProps (appContext) {
    try {
      const { req, res } = appContext.ctx
      const language = process.browser ? i18n.language : req.language

      appContext.ctx.stores = initializeStores(null)
      appContext.ctx.language = language
      appContext.ctx.router = appContext.router

      if (!process.browser) {
        setGlobalAppLocale(language)
      }

      const { authStore } = appContext.ctx.stores
      const { isAuthenticated } = authStore

      if (!process.browser && !isAuthenticated) {
        const userToken = req.headers.Authorization || getToken(req)
        const userId = getId(req)
        const cartAmount = getAmount(req)

        if (userToken && userId) {
          // Tìm thấy userToken
          // và Tìm thấy userId trong Cookie
          // => Kiểm tra thông tin userToken và lấy Profile của userId
          setGlobalAuthorization(userToken)

          const profileRes = await accountAPI.getProfile(userId)
          if (profileRes.success) {
            authStore.authSuccess({ profile: profileRes.data })
            authStore.setAmountCart(cartAmount)
            await Promise.all([
            ])
          } else {
            setGlobalAuthorization('')
            saveToken('', res)
            authStore.setAmountCart(0)
          }
        }
      }

      const appProps = App.getInitialProps
        ? await App.getInitialProps(appContext)
        : {}

      return {
        ...appProps,
        language,
        initialStores: appContext.ctx.stores
      }
    } catch (error) {
      console.log('_app.js -> getInitialProps -> error', error)

      return {
        pageProps: {
          namespacesRequired: ['global'],
          statusCode: 500
        }
      }
    }
  }

  constructor (props) {
    super(props)

    const isServer = typeof window === 'undefined'
    this.stores = isServer
      ? props.initialStores
      : initializeStores(props.initialStores)
  }

  componentDidMount () {
    const Wow = require('wow.js')
    new Wow().init()

    // Google Analytics
    if (process.env.NODE_ENV === 'production') {
      if (!window.GA_INITIALIZED) {
        const GG_ANALYTIC_ID = env.default.GG_ANALYTICS_ID
        if (GG_ANALYTIC_ID) {
          ReactGA.initialize(GG_ANALYTIC_ID)
          window.GA_INITIALIZED = true
        }
      }
    }

    if (LOG_MOBX_STORE) {
      autorun(() => {
        console.log('----------LOG MOBX STORE START----------')
        Object.keys(this.stores).map(key =>
          console.log(key, ': ', toJS(this.stores[key]))
        )
        console.log('----------LOG MOBX STORE END----------')
        console.log('\n')
      })
    }
  }

  render () {
    const { Component, pageProps } = this.props

    if (pageProps.statusCode) {
      return <Error statusCode={pageProps.statusCode} />
    }

    return (
      <>
        <Head>
          <meta
            name='viewport'
            content='width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no'
          />
        </Head>

        <Provider {...this.stores}>
          <GridThemeProvider gridTheme={gridTheme}>
            <Component {...pageProps} />
          </GridThemeProvider>
        </Provider>
      </>
    )
  }
}

export default appWithTranslation(MyApp)
