import React, { useEffect, useState } from "react";
// import { routerLink, routerUtils } from '../router'
import MainLayout from "../components/layouts";
import MainCarousel from "../components/carousel";
import Clearfix from "../components/clearfix";
import Heading from "../components/heading";
import Step from "../components/step";
import SaleList from "../components/grid-list-sales";
import ProductList from "../components/card-grid-products";
// import FakeLoading from '../components/fake-loading'
import Products from "../components/list-products-scroll";

import { notification, Divider, message, BackTop } from "antd";
import { UpOutlined } from "@ant-design/icons";
import { inject, observer } from "mobx-react";

const listData = [];
for (let i = 0; i < 14; i++) {
  listData.push({
    title: `Product - ${i}`,
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
  });
}

const styleBacktop = {
  height: 40,
  width: 40,
  lineHeight: "40px",
  borderRadius: 4,
  backgroundColor: "gray",
  color: "#fff",
  textAlign: "center",
  fontSize: 14,
};

const HomePage = ({ authStore }) => {
  const [products, setProducts] = useState([]);
  const [movies, setMovies] = useState([]);
  const [loading, setLoading] = useState(false);
  const [hasMore, setHasMore] = useState(true);
  const [page, setPage] = useState(0);

  const { isAuthenticated } = authStore;

  const deadline = Date.now() + 1000 * 60 * 60 * 24 * 2 + 1000 * 30; // Moment is also OK

  const fetchMoreData = () => {
    setLoading(true);

    if (products.length >= listData.length) {
      message.success("Full List Product");
      setHasMore(false);
      setLoading(false);
      return;
    }

    const tmp = page + 1;
    const newData = [...products, ...listData.slice(tmp * 5, tmp * 5 + 5)];

    setTimeout(() => {
      setProducts(newData);
      setLoading(false);
      setPage(tmp);
    }, 2000);
  };

  const openNotification = (placement) => {
    !isAuthenticated
      ? notification.open({
          message: "WELCOME TO CENTRAL CINEMA",
          className: "custom-class",
          style: {
            width: 400,
          },
          placement,
        })
      : notification.open({
          message: "HI, WELCOME BACK",
          description: "Find your best experience.",
          className: "custom-class",
          style: {
            width: 400,
          },
          placement,
        });
  };

  useEffect(() => {
    const noti = () => openNotification("topRight");
    const fetchData = () => {
      const newData = [...products, ...listData.slice(page * 5, page * 5 + 5)];
      setProducts(newData);
    };
    // const fetchSaleData = async () => {
    //   const url = `https://api.themoviedb.org/3/search/movie?api_key=${API_KEY}&query=A`;
    //   const response = await fetch(url);
    //   const data = await response.json();
    //   await setMovies(data);
    // };

    setTimeout(noti(), 0);
    setTimeout(fetchData, 0);
  }, []);

  return (
    <>
      <MainLayout title="CENTRAL CINEMA - HCM">
        <Clearfix height={32} />
        <MainCarousel />
        <Step />
        <Heading
          title="MOVIES THAT ARE PLAYING"
          level={3}
          description="BEST CHOICES"
          // countdown={deadline}
        />{" "}
        <SaleList />
        <Divider style={{ color: "#fff" }} />
        <Heading
          title="UPCOMING MOVIES"
          level={3}
          description="FOR NEXT WEEK"
          tags={[{ id: 1, name: "Upcoming" }]}
        />{" "}
        <ProductList />
        {/* <Divider style={{ color: "#fff" }} /> */}
        {/* <Heading
        title="EVENTS"
        level={3}
        tags={[{ id: 1, name: "Events" }]}
      />{" "} */}
        {/* <FakeLoading /> */}
        {/* <Products
        data={products}
        fetchMoreItem={fetchMoreData}
        loading={loading}
        hasMore={hasMore}
      /> */}
      </MainLayout>
      <BackTop visibilityHeight={200}>
        <div style={styleBacktop}>
          <UpOutlined />
        </div>
      </BackTop>
    </>
  );
};

HomePage.getInitialProps = async (ctx) => {
  // routerUtils.redirect(ctx, routerLink.home.index.get().linkProps())

  return {
    namespacesRequired: ["global"],
  };
};

export default inject("authStore")(observer(HomePage));
