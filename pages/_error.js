import { Result, Button } from 'antd'

function Error ({ statusCode }) {
  return (
    <Result
      status={statusCode}
      title={statusCode}
      subTitle='Sorry, you are not authorized to access this page.'
      extra={<Button type='primary'>Back Home</Button>}
    />
  )
}

Error.getInitialProps = ({ res, err }) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404
  return {
    namespacesRequired: ['global'],
    statusCode
  }
}

export default Error
