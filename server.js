const express = require('express')
const next = require('next')
const path = require('path')
const { router } = require('./router')

const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production'

if (dev) {
  require('dotenv').config()
}

const app = next({ dev })
const handle = app.getRequestHandler()

const customServer = async () => {
  await app.prepare()
  const server = express()

  server.use('/locales', express.static(path.join(__dirname, '/locales')))

  const isMobile = (req) => {
    const ua = req.header('user-agent')
    const isMobile = /mobile/i.test(ua)
    return isMobile
  }

  const createRouterPath = routerPath => {
    server.get(routerPath.as, (req, res) => {
      app.render(req, res, routerPath.file, {
        ...req.params,
        ...req.query,
        isMobile: isMobile(req)
      })
    })
  }

  // Auth
  // createRouterPath(router.auth.index)
  // createRouterPath(router.auth.login)

  // Home
  createRouterPath(router.home.index)

  server.all('*', (req, res) => {
    return handle(req, res)
  })

  server.listen(port, err => {
    if (err) throw err
    console.log(`> The App ready on http://localhost:${port} by decoding team.`)
  })
}

// noinspection JSIgnoredPromiseFromCall
customServer()
