import React, { useState } from 'react'
import { List, Space, Spin } from 'antd'
import { ShoppingOutlined, StarOutlined } from '@ant-design/icons'

import DrawerProduct from '../drawer-info-product'

import InfiniteScroll from 'react-infinite-scroller'

const IconText = ({ icon, text }) => (
  <Space>
    {React.createElement(icon)}
    {text}
  </Space>
)

const ListProducts = ({ data = [], loading, fetchMoreItem, hasMore }) => {
  const [visible, setVisible] = useState(false)

  const onClose = () => {
    setVisible(false)
  }

  return (
    <>
      <InfiniteScroll
        initialLoad={false}
        loadMore={fetchMoreItem}
        hasMore={hasMore}
        loader={(
          <Spin style={{ display: 'block', marginLeft: 'auto', marginRight: 'auto' }} />
        )}
      >
        <List
          itemLayout='vertical'
          size='small'
          dataSource={data}
          style={{ margin: '48px' }}
          renderItem={item => (
            <List.Item
              key={item.title}
              actions={[
                <IconText icon={ShoppingOutlined} text='BUYED: 123' key='list-vertical-star-o' />,
                <IconText icon={StarOutlined} text='156' key='list-vertical-star-o' />
              ]}
              extra={
                <img
                  width={272}
                  style={{ borderRadius: '10px' }}
                  alt='logo'
                  src='https://miro.medium.com/max/1400/1*-2QdjaEpIGNeU0iUQd0OTQ.jpeg'
                />
              }
            >
              <List.Item.Meta
                // avatar={<Avatar src={item.avatar} />}
                title={<a href={item.href}>{item.title}</a>}
                description={item.description}
                onClick={() => setVisible(true)}
              />
              {item.content}
            </List.Item>
          )}
        />
      </InfiniteScroll>
      <DrawerProduct visible={visible} onClose={onClose} />
    </>
  )
}

export default ListProducts
