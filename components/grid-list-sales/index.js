import React from "react";
import { List, Card } from "antd";
import { useEffect, useState } from "react";
import DrawerProduct from "../../components/drawer-info-product";

const { Meta } = Card;

const gridStyle = {
  textAlign: "center",
};

const movies = [
  {
    adult: false,
    backdrop_path: "/c2cb0lLvbzvHlSUo35W8CfP0kYO.jpg",
    genre_ids: [53],
    id: 714968,
    original_language: "en",
    original_title: "Plan A",
    overview:
      "Germany 1945, Max, a Jewish Holocaust survivor, meets a radical group of Jewish resistance fighters, who, like him, lost all hope for their future after they were robbed of their existence and their entire families were killed by the Nazis. They dream of retaliation on an epic scale for the Jewish people. An eye for an eye, a tooth for a tooth. Max starts identifying with the group's monstrous plans...",
    popularity: 275.541,
    poster_path: "/dfYOmisdbiVTXdcQji35pKywPFI.jpg",
    release_date: "2021-08-16",
    title: "Plan A",
    video: false,
    vote_average: 5.4,
    vote_count: 17,
  },
  {
    adult: false,
    backdrop_path: "/5RMqFZdefnDwY7rdD1oJcTkWPdF.jpg",
    genre_ids: [16, 35, 10751],
    id: 774741,
    original_language: "en",
    original_title: "Diary of a Wimpy Kid",
    overview:
      "Greg Heffley is a scrawny but ambitious kid with an active imagination and big plans to be rich and famous – he just has to survive middle school first.",
    popularity: 1836.038,
    poster_path: "/obg6lWuNaZkoSlwrVG4VVk4SmT.jpg",
    release_date: "2021-12-03",
    title: "Diary of a Wimpy Kid",
    video: false,
    vote_average: 7.1,
    vote_count: 71,
  },
  {
    adult: false,
    backdrop_path: "/odKqOY6VE6C59YAdGHB0b5Havye.jpg",
    genre_ids: [10751, 12, 14],
    id: 615666,
    original_language: "en",
    original_title: "A Boy Called Christmas",
    overview:
      "An ordinary young boy called Nikolas sets out on an extraordinary adventure into the snowy north in search of his father who is on a quest to discover the fabled village of the elves, Elfhelm. Taking with him a headstrong reindeer called Blitzen and a loyal pet mouse, Nikolas soon meets his destiny in this magical and endearing story that proves nothing is impossible…",
    popularity: 588.848,
    poster_path: "/1sRejtiHOZGggZd9RcmdqbapLM5.jpg",
    release_date: "2021-11-25",
    title: "A Boy Called Christmas",
    video: false,
    vote_average: 7.8,
    vote_count: 225,
  },
  {
    adult: false,
    backdrop_path: "/8s4h9friP6Ci3adRGahHARVd76E.jpg",
    genre_ids: [16, 10751, 35, 878],
    id: 379686,
    original_language: "en",
    original_title: "Space Jam: A New Legacy",
    overview:
      "When LeBron and his young son Dom are trapped in a digital space by a rogue A.I., LeBron must get them home safe by leading Bugs, Lola Bunny and the whole gang of notoriously undisciplined Looney Tunes to victory over the A.I.'s digitized champions on the court. It's Tunes versus Goons in the highest-stakes challenge of his life.",
    popularity: 450.21,
    poster_path: "/5bFK5d3mVTAvBCXi5NPWH0tYjKl.jpg",
    release_date: "2021-07-08",
    title: "Space Jam: A New Legacy",
    video: false,
    vote_average: 7.2,
    vote_count: 2672,
  },
  {
    adult: false,
    backdrop_path: "/mWS0X8Zs04gwFcoTsgiuEmbFiAZ.jpg",
    genre_ids: [35],
    id: 611408,
    original_language: "fr",
    original_title: "Mystère à Saint-Tropez",
    overview:
      "Every summer, the Billionaire Tranchant and his wife Eliane host the glamorous world of celebrities in their luxurious mansion in the south of France. When a criminal car sabotage related to threatening letters wreak havoc in the villa, Tranchant goes in search of the best agent for the investigation. In the middle of this hot summer, only the arrogant and incompetent agent Boullin is available... To catch the suspect and solve the mystery, he will have no choice but to impersonate a newly hired butler and will turn everyone’s vacation into a hilarious game of ‘Cluedo’!",
    popularity: 111.6,
    poster_path: "/bWpIB5MWhH3vDmNUOC1PuE0ejt1.jpg",
    release_date: "2021-07-14",
    title: "Do You Do You Saint-Tropez",
    video: false,
    vote_average: 4.9,
    vote_count: 43,
  },
  {
    adult: false,
    backdrop_path: "/8fW0wqB3a6dvIoIBduCKocZi58a.jpg",
    genre_ids: [16],
    id: 864390,
    original_language: "en",
    original_title: "Mickey and Minnie Wish Upon a Christmas",
    overview:
      "After a series of mishaps, Mickey, Minnie and the gang are separated all over the world and must try to get back to Hot Dog Hills by Christmas Eve. A mysterious and jolly stranger shows up to tell them about The Wishing Star, which could be the secret to bringing everybody home in time to celebrate together.",
    popularity: 388.242,
    poster_path: "/viZ3SvvQi7CRHJxnSKAG2lHU4AK.jpg",
    release_date: "2021-12-02",
    title: "Mickey and Minnie Wish Upon a Christmas",
    video: false,
    vote_average: 7.2,
    vote_count: 29,
  },
  {
    adult: false,
    backdrop_path: "/fmIp40ev4VGquK2bMo52PQgaV2d.jpg",
    genre_ids: [16, 10751, 12, 14, 35],
    id: 529203,
    original_language: "en",
    original_title: "The Croods: A New Age",
    overview:
      "Searching for a safer habitat, the prehistoric Crood family discovers an idyllic, walled-in paradise that meets all of its needs. Unfortunately, they must also learn to live with the Bettermans -- a family that's a couple of steps above the Croods on the evolutionary ladder. As tensions between the new neighbors start to rise, a new threat soon propels both clans on an epic adventure that forces them to embrace their differences, draw strength from one another, and survive together.",
    popularity: 317.531,
    poster_path: "/tbVZ3Sq88dZaCANlUcewQuHQOaE.jpg",
    release_date: "2020-11-25",
    title: "The Croods: A New Age",
    video: false,
    vote_average: 7.6,
    vote_count: 2692,
  },
  {
    adult: false,
    backdrop_path: "/hwwFyowfcbLRVmRBOkvnABBNIOs.jpg",
    genre_ids: [12, 16, 35, 10751],
    id: 9487,
    original_language: "en",
    original_title: "A Bug's Life",
    overview:
      'On behalf of "oppressed bugs everywhere," an inventive ant named Flik hires a troupe of warrior bugs to defend his bustling colony from a horde of freeloading grasshoppers led by the evil-minded Hopper.',
    popularity: 68.913,
    poster_path: "/hFamOus53922agTlKxhcL7ngJ9h.jpg",
    release_date: "1998-11-25",
    title: "A Bug's Life",
    video: false,
    vote_average: 7,
    vote_count: 7335,
  },
];

const ListProduct = () => {
  const [visible, setVisible] = useState(false);
  const [info, setInfo] = useState({
    title: "",
    overview: "",
    poster_path: "",
    id: "",
    vote_average: "",
    vote_count: "",
  });

  const onClose = () => {
    setVisible(false);
  };

  return movies ? (
    <>
      <List
        style={{ margin: "24px" }}
        itemLayout="vertical"
        grid={{
          gutter: 16,
          xs: 1,
          sm: 2,
          md: 4,
          lg: 4,
          xl: 6,
          xxl: 3,
        }}
        pagination={{
          onChange: (page) => {
            console.log(page);
          },
          pageSize: 6,
        }}
        dataSource={movies}
        renderItem={(item) => (
          <>
            <List.Item>
              <Card
                hoverable
                style={{ height: 740 }}
                cover={
                  <img
                    alt={item.title}
                    src={`https://image.tmdb.org/t/p/w185${item.poster_path}`}
                    style={{ height: 480, objectFit: "cover" }}
                  />
                }
                onClick={() => {
                  setVisible(true);
                  setInfo(item);
                }}
              >
                <Meta title={item.title} description={item.overview} />
              </Card>
              ,{" "}
            </List.Item>
          </>
        )}
      />
      <DrawerProduct
        visible={visible}
        onClose={onClose}
        title={info.title}
        overview={info.overview}
        poster_path={info.poster_path}
        id={info.id}
        vote_average={info.vote_average}
        vote_count={info.vote_count}
      />
    </>
  ) : (
    <></>
  );
};

export default ListProduct;
