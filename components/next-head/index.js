import Head from "next/head";
import PropTypes from "prop-types";
import React from "react";
import env from "env";
import { useRouter } from "next/router";

const siteTitle = "CENTRAL CINEMA";
const siteLogo = "/images/logo.png";
const siteDescription = "Find Your Best Experience";
const siteKeywords = "CENTRAL CINEMA";
const siteCover = "/images/site-cover.png";

const NextHead = ({
  title,
  type = "website",
  keywords,
  description,

  author = "Huu Dang",
  image,
  imageWidth = 1200,
  imageHeight = 630,

  children,
}) => {
  const router = useRouter();

  const pageTitle = title ? `${title} - ${siteTitle}` : siteTitle;

  const url = env.WEB_URL + router.asPath;

  return (
    <Head>
      <title>{pageTitle}</title>
      <meta name="description" content={description || siteDescription} />
      <meta name="keywords" content={keywords || siteKeywords} />
      <meta name="author" content={author} />

      <meta property="og:type" content={type} />
      <meta property="og:url" content={url} />
      <meta property="og:title" content={pageTitle} />
      <meta property="og:site_name" content={siteTitle} />
      <meta
        property="og:description"
        content={description || siteDescription}
      />

      <meta property="og:image" content={image || siteCover} />
      <meta property="og:image:alt" content={pageTitle} />
      <meta property="og:image:width" content={imageWidth} />
      <meta property="og:image:height" content={imageHeight} />

      <link rel="shortcut icon" type="image/png" href={siteLogo} />

      {children}
    </Head>
  );
};

NextHead.propTypes = {
  title: PropTypes.string,
  type: PropTypes.string,
  keywords: PropTypes.string,
  description: PropTypes.string,

  author: PropTypes.string,
  image: PropTypes.string,
  imageWidth: PropTypes.number,
  imageHeight: PropTypes.number,
};

export default NextHead;
