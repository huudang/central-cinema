import React from 'react'
import { Space, Skeleton, Spin } from 'antd'

const FakeLoading = () => {
  return (
    <>
      <Space style={{ margin: '80px' }}>
        <Skeleton.Button active='true' size='small' shape='circle' />
        <Skeleton.Button active='true' size='small' shape='circle' />
        <Skeleton.Avatar active size='small' shape='circle' />
        <Skeleton.Input style={{ width: 200 }} active size='small' />
      </Space>
      {/* <Spin /> */}
    </>
  )
}

export default FakeLoading
