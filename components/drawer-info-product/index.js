import React, { useState } from "react";
import Link from "next/link";
import { routerLink } from "../../router";
import axios from "axios";

import {
  Drawer,
  Row,
  Col,
  Divider,
  Button,
  Typography,
  Rate,
  InputNumber,
  message,
} from "antd";
import { inject, observer } from "mobx-react";

const { Text, Title } = Typography;

const DrawerProduct = ({
  onClose,
  visible,
  authStore,
  title,
  poster_path,
  overview,
  id,
  vote_average,
  vote_count,
  disabled = false,
}) => {
  const success = () => {
    message.success("Book ticket success");
  };

  const handleAddToCart = () => {
    onClose();
    if (isAuthenticated) {
      authStore.addToCart();
      axios.post("http://localhost:1337/bookings", {
        fullname: me.username,
        phone: me.phone,
        movieName: title,
        overview: overview,
        price: vote_count,
        amount: 1,
      });
      success();
    }
  };

  const { isAuthenticated, me } = authStore;

  return (
    <>
      <Drawer
        title="Movie Info"
        width={880}
        placement="right"
        closable={false}
        onClose={onClose}
        visible={visible}
        bodyStyle={{ paddingBottom: 80 }}
        footer={
          !isAuthenticated ? (
            <Link {...routerLink.auth.login.get().linkProps()}>
              <Button
                htmlType="submit"
                type="primary"
                block
                onClick={handleAddToCart}
                disabled={disabled}
              >
                BUY IT
              </Button>
            </Link>
          ) : (
            <Button
              htmlType="submit"
              type="primary"
              block
              onClick={handleAddToCart}
              disabled={disabled}
            >
              BUY IT
            </Button>
          )
        }
      >
        <Title level={2} style={{ marginBottom: 24 }}>
          {title}.
        </Title>
        <Row>
          <Col span={12}>
            <img
              alt={title}
              src={`https://image.tmdb.org/t/p/w185${poster_path}`}
              style={{
                objectFit: "contain",
                width: "95%",
                height: "auto",
                borderRadius: "8px",
              }}
            />
          </Col>
          <Col span={12}>
            <Title type="danger" strong level={3}>
              {vote_count}$
            </Title>
            <Text code copyable>
              Code: #{id}
            </Text>
            <Divider />
            <Title mark level={5}>
              Save 10%
            </Title>
            {/* <Col span={12}> */}
            <Title level={5}>{overview}</Title>
            {/* </Col> */}
            <InputNumber min={1} max={10} defaultValue={1} bordered={false} />
          </Col>
        </Row>

        <Divider />

        <Row>
          <Col span={12}>
            <Rate disabled defaultValue={(vote_average / 10) * 5} />
          </Col>
          <Col span={12}></Col>
        </Row>
        <Row style={{ marginTop: "48px" }}>
          <Col span={6}>
            <img
              alt="example"
              src="/images/carousel/Group-3.svg"
              style={{
                objectFit: "contain",
                width: "95%",
                height: "auto",
                borderRadius: "8px",
              }}
            />
          </Col>
          <Col span={6}>
            <img
              alt="example"
              src="/images/carousel/Group-2.svg"
              style={{
                objectFit: "contain",
                width: "95%",
                height: "auto",
                borderRadius: "8px",
              }}
            />
          </Col>
          <Col span={5}>
            <img
              alt="example"
              src="/images/carousel/Group-1.svg"
              style={{
                objectFit: "contain",
                width: "95%",
                height: "auto",
                borderRadius: "8px",
              }}
            />
          </Col>
          <Col span={6}>
            <img
              alt="example"
              src="/images/carousel/Group.svg"
              style={{
                objectFit: "contain",
                width: "95%",
                height: "auto",
                borderRadius: "8px",
              }}
            />
          </Col>
        </Row>
      </Drawer>
    </>
  );
};

export default inject("authStore")(observer(DrawerProduct));
