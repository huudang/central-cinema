import { DatePicker } from 'components/antd'
import moment from 'moment'

const DateRangePicker = ({
  name,
  onChange,
  onBlur,

  isFullWidth,
  autoFocus,

  ...otherProps
}) => {
  const handleChange = (value) => {
    const event = {
      target: {
        name,
        value
      }
    }

    onChange && onChange(event)
  }

  const handleBlur = () => {
    const event = {
      target: {
        name
      }
    }

    onBlur && onBlur(event)
  }

  return (
    <DatePicker.RangePicker
      style={isFullWidth ? { width: '100%' } : {}}

      format='DD-MM-YYYY'
      ranges={{
        'Hôm nay': [moment(), moment()],
        'Tuần này': [moment().startOf('week'), moment().endOf('week')],
        'Tháng này': [moment().startOf('month'), moment().endOf('month')]
      }}

      placeholder={['Ngày bắt đầu', 'Ngày kết thúc']}
      onChange={handleChange}
      onBlur={handleBlur}

      autoFocus={autoFocus}

      {...otherProps}
    />
  )
}

export default DateRangePicker
