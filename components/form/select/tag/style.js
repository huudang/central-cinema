import styled from 'styled-components'

export const S4OptionList = styled.div`
  &.horizontal {
    display: flex;
    align-items: center;
    flex-wrap: wrap;
    margin-bottom: -8px;
  }
`
