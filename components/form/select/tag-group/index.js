import { S4Group, S4OptionHeading, S4OptionList } from './style'

import OptionItem from '../tag/option-item'
import PropTypes from 'prop-types'
import useSelectTag from 'hooks/use-select-tag'

const SelectTagGroup = ({
  mode = 'single',
  options = [],

  name,
  value,
  defaultValue,

  onChange,

  ...props
}) => {
  const {
    checkSelected,
    onOptionClick
  } = useSelectTag({
    mode,
    name,
    value,
    defaultValue,
    onChange
  })

  return (
    <S4OptionList>
      {options.map(group => {
        return (
          <S4Group key={group.name}>
            <S4OptionHeading>
              <label>
                {group.name}
              </label>
            </S4OptionHeading>

            {group.options.map(option => {
              const isSelected = checkSelected(option.value)

              return (
                <OptionItem
                  key={option.value}
                  direction='vertical'

                  icon={option.icon}
                  label={option.label}
                  value={option.value}

                  isSelected={isSelected}
                  disabled={option.disabled}
                  onClick={onOptionClick}
                />
              )
            })}
          </S4Group>
        )
      })}
    </S4OptionList>
  )
}

const OptionValueType = PropTypes.oneOfType([PropTypes.string, PropTypes.number])

const ValueType = PropTypes.oneOfType([
  PropTypes.string,
  PropTypes.number,

  // { value: "vi" }
  PropTypes.shape({
    value: OptionValueType
  }),

  // ["vi", "en"]
  // or [0, 1]
  PropTypes.arrayOf(OptionValueType),

  // [{ value: "vi" }, { value: "en" }]
  // or [{ value: 0 }, { value: 1 }]
  PropTypes.arrayOf(PropTypes.shape({
    value: OptionValueType
  }))
])

SelectTagGroup.propTypes = {
  options: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string,
    options: PropTypes.arrayOf(PropTypes.shape({
      icon: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.string
      ]),

      label: PropTypes.string,
      value: OptionValueType,

      description: PropTypes.string
    }))
  })),

  defaultValue: ValueType,
  value: ValueType,

  onChange: PropTypes.func,
  mode: PropTypes.oneOf(['single', 'multiple'])
}

export default SelectTagGroup
