import styled from 'styled-components'
import themeConstants from 'theme/constants'

export const S4Group = styled.div`
  margin-bottom: 16px;
`

export const S4OptionList = styled.div`
  &.horizontal {
    display: flex;
    align-items: center;
    flex-wrap: wrap;
    margin-bottom: -8px;
  }
`

export const S4OptionHeading = styled.div`
  padding: 8px 16px;
  margin-bottom: 8px;
  background: ${themeConstants.color.primaryTransparent[100]};
  border-radius: 4px;

  label {
    font-weight: ${themeConstants.fontWeights.medium};
    color: ${themeConstants.text.primaryColor};
  }
`
