import Dropdown from './dropdown'
import Tag from './tag'
import TagGroup from './tag-group'

const Select = {
  Dropdown,
  Tag,
  TagGroup
}

export default Select
