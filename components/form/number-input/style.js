import styled from 'styled-components'
import themeConstants from 'theme/constants'

export const S4NumberInput = styled.div`
  display: inline-flex;
  align-items: center;

  .ant-btn.ant-btn-sm {
    min-width: 24px;
    height: 24px;

    display: flex;
    align-items: center;
    justify-content: center;

    &[disabled] {
      background: #d9d9d9;
      color: #fff;
    }
  }
`

export const S4Number = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 40px;
  font-size: 16px;
  font-weight: ${themeConstants.fontWeights.medium};
`
