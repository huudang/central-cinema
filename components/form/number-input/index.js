import { S4Number, S4NumberInput } from './style'

import { Button } from 'components/antd'
import PropTypes from 'prop-types'
import React from 'react'

const NumberInput = ({ name, value = 0, onChange }) => {
  const handleChange = (value) => {
    onChange && onChange({
      target: {
        name,
        value
      }
    })
  }

  const handleDec = () => {
    handleChange(value - 1)
  }

  const handleInc = () => {
    handleChange(value + 1)
  }

  return (
    <S4NumberInput>
      <Button shape='circle' type='primary' size='small' disabled={value === 0} onClick={handleDec}>
        <i className='fas fa-minus' />
      </Button>

      <S4Number>
        {value}
      </S4Number>

      <Button shape='circle' type='primary' size='small' onClick={handleInc}>
        <i className='fas fa-plus' />
      </Button>
    </S4NumberInput>
  )
}

NumberInput.propTypes = {
  name: PropTypes.string,
  value: PropTypes.number,
  onChange: PropTypes.func
}

export default NumberInput
