import { InfoCircleFilled } from '@ant-design/icons'
import React from 'react'
import styled from 'styled-components'
import classNames from 'classnames'

const S4ErrorMessage = styled.div`
`

const FormErrorMessage = ({ className, children }) => {
  if (typeof children !== 'string') {
    return null
  }

  return (
    <S4ErrorMessage className={classNames('margin-top-8 text-danger', className)}>
      <InfoCircleFilled />
      <span className='margin-left-8'>{children}</span>
    </S4ErrorMessage>
  )
}

export default FormErrorMessage
