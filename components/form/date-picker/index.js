import { DatePicker as AntdDatePicker } from 'components/antd'
import styled from 'styled-components'
import classNames from 'classnames'
import { S4Prefix } from '../money-input'

const S4DatePicker = styled.div`
  position: relative;

  &.block {
    .ant-picker {
      width: 100%;
    }
  }

  &.with-icon {
    .ant-picker {
      padding-left: ${16 + 14 + 15}px;
    }
  }
`

const DatePicker = ({
  className,
  style,

  name,
  onChange,
  onBlur,

  icon,
  block,
  autoFocus,

  ...otherProps
}) => {
  const handleChange = (value) => {
    const event = {
      target: {
        name,
        value
      }
    }

    onChange && onChange(event)
  }

  const handleBlur = () => {
    const event = {
      target: {
        name
      }
    }

    onBlur && onBlur(event)
  }

  return (
    <S4DatePicker
      className={classNames(className, { 'with-icon': icon, block: block })}
      style={style}
    >
      {icon && (
        <S4Prefix />
      )}

      <AntdDatePicker
        onChange={handleChange}
        onBlur={handleBlur}

        format='DD-MM-YYYY'
        autoFocus={autoFocus}

        {...otherProps}
      />
    </S4DatePicker>
  )
}

export default DatePicker
