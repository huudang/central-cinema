import { Radio as AntdRadio } from 'components/antd'
import PropTypes from 'prop-types'

const radioStyle = {
  display: 'flex',
  alignItems: 'center',
  height: 32
}

const Radio = ({
  options = [],
  name,
  value,
  onChange
}) => {
  return (
    <AntdRadio.Group onChange={onChange} name={name} value={value}>
      {options.map(option => (
        <AntdRadio
          key={option.value}
          value={option.value}
          style={radioStyle}
        >
          {option.label}
        </AntdRadio>
      ))}
    </AntdRadio.Group>
  )
}

Radio.propTypes = {
  options: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.any
  })),

  name: PropTypes.string,
  value: PropTypes.any,
  onChange: PropTypes.func
}

export default Radio
