import { Input, Typography } from 'components/antd'
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng
} from 'react-places-autocomplete'

import React from 'react'
import styled from 'styled-components'
import themeConstants from 'theme/constants'

const { Text } = Typography

const S4SuggestionList = styled.div`
  position: absolute;
  top: 40px;
  left: 0;
  z-index: ${themeConstants.zIndex.dropdown};

  width: 100%;
  border-radius: ${themeConstants.borderRadius.md};
  box-shadow: ${themeConstants.shadow.md};

  background: #fff;
`

const S4SuggestItem = styled.div`
  display: flex;
  align-items: center;

  height: 40px;
  padding: 0 16px;
  border-bottom: 1px solid ${themeConstants.border.defaultColor};

  cursor: pointer;
`

const LocationSearchInput = ({
  id,
  name,
  value,
  onChange,
  onBlur,
  onGetLatLng,
  placeholder,
  prefix,
  suffix
}) => {
  const handleBlur = () => {
    onBlur && onBlur({
      target: {
        name
      }
    })
  }

  const handleChange = (address) => {
    onChange && onChange({
      target: {
        name,
        value: address
      }
    })
  }

  const handleSelect = async (address) => {
    handleChange(address)

    try {
      const results = await geocodeByAddress(address)
      const result = results[0]
      const latLng = await getLatLng(result)
      const addressInfo = { ...latLng, id: result.place_id } // lat, lng, id
      console.log('LocationSearchInput', address, '->', addressInfo)
      onGetLatLng && onGetLatLng(addressInfo)
    } catch (error) {
      console.log('Error', error)
    }
  }

  const handleError = (error) => {
    console.log('Error', error)
  }

  return (
    <PlacesAutocomplete
      value={value}
      onChange={handleChange}
      onSelect={handleSelect}
      onError={handleError}
    >
      {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
        <div style={{ position: 'relative' }}>
          <Input
            {...getInputProps({
              id,
              name,
              onBlur: handleBlur,
              placeholder,
              prefix,
              suffix,
              className: 'location-search-input'
            })}
          />

          <S4SuggestionList>
            {suggestions.map((suggestion) => {
              const className = suggestion.active
                ? 'active'
                : ''

              const props = getSuggestionItemProps(suggestion, {
                className
              })

              return (
                <S4SuggestItem
                  {...props}
                  key={suggestion.placeId}
                >
                  <Text ellipsis>
                    {suggestion.description}
                  </Text>
                </S4SuggestItem>
              )
            })}
          </S4SuggestionList>
        </div>
      )}
    </PlacesAutocomplete>
  )
}

export default LocationSearchInput
