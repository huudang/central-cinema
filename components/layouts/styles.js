import styled from 'styled-components'

import { media } from 'styled-bootstrap-grid'
import themeConstants from '../../theme/constants'

export const S4MainNav = styled.div`
  max-width: @full-box-width;
  width: 100%;
  margin: 0 auto;
  padding: 0 16px;
`

export const S4LeftNav = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: space-between;
  width: 80px;
`

export const S4A = styled.div`
  color: @menu-item-color;
`

export const S4Footer = styled.footer`
  padding: 32px 0;
  border-top: 1px solid ${themeConstants.border.defaultColor};
`

export const Copyright = styled.span`
  display: flex;
  font-weight: ${themeConstants.fontWeights.medium};
`

export const Contact = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 16px;

  ${media.md`
    flex-direction: row;
    align-items: center;
  `}

  ${media.lg`
    margin-top: 0;
    justify-content: flex-end;
  `}

  .contact-item {
    height: 40px;
    display: flex;
    align-items: center;

    padding: 0 16px;
    margin-bottom: 8px;
    border-radius: 20px;

    background: ${themeConstants.color.gray};
    font-weight: ${themeConstants.fontWeights.medium};

    ${media.md`
      margin-bottom: 0;
    `}

    &:not(:last-child) {
      margin-right: 16px;
    }
    i {
      margin-right: 4px;
    }
  }
`

export const Line = styled.div`
  border-top: 1px solid ${themeConstants.border.defaultColor};
  margin-top: 16px;
  margin-bottom: 32px;
`
