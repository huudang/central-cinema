import React from "react";
import Link from "next/link";
import {
  Layout,
  Row,
  Col,
  Badge,
  Menu,
  Input,
  Space,
  AutoComplete,
} from "antd";
import { HeartOutlined, ShakeOutlined } from "@ant-design/icons";
import { inject, observer } from "mobx-react";
import AutoCompleteComp from "./autocomplete";

const { Header } = Layout;

const MainNav = ({ currentTab = "1", authStore }) => {
  const { isAuthenticated, cartAmount } = authStore;

  return (
    <Header
      style={{
        position: "fixed",
        zIndex: 1,
        width: "100%",
        backgroundColor: "#fff",
      }}
    >
      <Row justify="space-between">
        <Col span={5}>
          <Menu
            theme="light"
            mode="horizontal"
            defaultSelectedKeys={[currentTab]}
          >
            <Menu.Item key="1">
              <Link href="/">CENTRAL MEDIA</Link>
            </Menu.Item>
            {/* <Menu.Item key="2">
              <Link href="/products/all">MOVIES</Link>
            </Menu.Item> */}
            {/* <Menu.Item key="3">
              <Link href="/accessories">THEATER CLUSTER</Link>
            </Menu.Item> */}
          </Menu>
        </Col>
        <Col span={6}>
          <AutoCompleteComp />
        </Col>
        <Col span={4} style={{ textAlign: "right" }}>
          <Space size="middle">
            {/* <Link href="/cart"> */}
            <Badge
              count={cartAmount}
              style={{
                backgroundColor: "#fff",
                color: "red",
                boxShadow: "0 0 0 1px #d9d9d9 inset",
              }}
            >
              <ShakeOutlined style={{ fontSize: 25, cursor: "pointer" }} />
            </Badge>
            {/* </Link> */}
          </Space>
        </Col>
      </Row>
    </Header>
  );
};

export default inject("authStore")(observer(MainNav));
