import React from "react";
import { Steps, Row } from "antd";
import Link from "next/link";
import { inject, observer } from "mobx-react";
import { routerLink } from "../../router";

const { Step } = Steps;

const StepRender = ({ currentStatus = 1, authStore }) => {
  const { isAuthenticated, cartAmount } = authStore;

  if (isAuthenticated) currentStatus = 2;

  return (
    <Row justify="center" style={{ margin: "80px" }}>
      <Steps size="small" current={currentStatus}>
        {!cartAmount ? (
          <Step status="wait" title="FIND YOUR MOVIE" />
        ) : (
          <Step status="finish" title="FIND YOUR MOVIE" />
        )}
        {!isAuthenticated ? (
          <Step
            status="wait"
            title={
              <Link {...routerLink.auth.login.get().linkProps()}>
                FILL INFORMATION
              </Link>
            }
          />
        ) : (
          <Step status="finish" title="FILL INFORMATION" />
        )}
        <Step status="wait" title="PAY" />
        <Step status="wait" title="SEE TOGETHER" />
      </Steps>
    </Row>
  );
};

export default inject("authStore")(observer(StepRender));
