import React, { useState } from "react";
import { Card, List } from "antd";
import DrawerProduct from "../../components/drawer-info-product";

const { Meta } = Card;

const data = [
  {
    adult: false,
    backdrop_path: "/iUeeZ5PWfZGgUtCJfwcgmCfdzoI.jpg",
    genre_ids: [16, 878, 10751, 35],
    id: 482321,
    original_language: "en",
    original_title: "Ron's Gone Wrong",
    overview:
      "In a world where walking, talking, digitally connected bots have become children's best friends, an 11-year-old finds that his robot buddy doesn't quite work the same as the others do.",
    popularity: 1917.981,
    poster_path: "/gA9QxSravC2EVEkEKgyEmDrfL0e.jpg",
    release_date: "2021-10-15",
    title: "Ron's Gone Wrong",
    video: false,
    vote_average: 8.5,
    vote_count: 343,
  },
  {
    adult: false,
    backdrop_path: "/2oxlor4lQrP2JX40ammAQdOqkEQ.jpg",
    genre_ids: [53, 80, 878, 9648],
    id: 17532,
    original_language: "en",
    original_title: "S. Darko",
    overview:
      "S. Darko follows Samantha Darko, the younger sister of Donnie, the protagonist of Donnie Darko, and her friend Corey. On their way to California, their car breaks down, forcing them to wait in a small town until it is fixed. While there, Samantha begins to have dreams that warn her of the end of the universe.",
    popularity: 17.43,
    poster_path: "/aRYrV4EloKRgv0lCqzGtXFt3dXn.jpg",
    release_date: "2009-04-28",
    title: "S. Darko",
    video: false,
    vote_average: 4.1,
    vote_count: 331,
  },
  {
    adult: false,
    backdrop_path: "/zSLD94ofBzMShLgWcbeEfTa5zJ7.jpg",
    genre_ids: [28, 35, 53],
    id: 522931,
    original_language: "en",
    original_title: "Hitman's Wife's Bodyguard",
    overview:
      "The world’s most lethal odd couple – bodyguard Michael Bryce and hitman Darius Kincaid – are back on another life-threatening mission. Still unlicensed and under scrutiny, Bryce is forced into action by Darius's even more volatile wife, the infamous international con artist Sonia Kincaid. As Bryce is driven over the edge by his two most dangerous protectees, the trio get in over their heads in a global plot and soon find that they are all that stand between Europe and a vengeful and powerful madman.",
    popularity: 69.388,
    poster_path: "/6zwGWDpY8Zu0L6W4SYWERBR8Msw.jpg",
    release_date: "2021-06-14",
    title: "Hitman's Wife's Bodyguard",
    video: false,
    vote_average: 7,
    vote_count: 948,
  },
  {
    adult: false,
    backdrop_path: "/uWbP41poWgP0f7sgAQTf6UeRtwg.jpg",
    genre_ids: [27],
    id: 753453,
    original_language: "en",
    original_title: "V/H/S/94",
    overview:
      "A mysterious VHS tape leads a police S.W.A.T. team to the discovery of a sinister cult whose collection of pre-recorded material reveals a nightmarish conspiracy.",
    popularity: 36.655,
    poster_path: "/9fSogEkqDfJQTuGHsTFloQz3Tw8.jpg",
    release_date: "2021-09-26",
    title: "V/H/S/94",
    video: false,
    vote_average: 6.6,
    vote_count: 156,
  },
  {
    adult: false,
    backdrop_path: "/iP0capCU4w5nxCDl398CH7yR1r9.jpg",
    genre_ids: [12, 14, 28],
    id: 9543,
    original_language: "en",
    original_title: "Prince of Persia: The Sands of Time",
    overview:
      "A rogue prince reluctantly joins forces with a mysterious princess and together, they race against dark forces to safeguard an ancient dagger capable of releasing the Sands of Time – gift from the gods that can reverse time and allow its possessor to rule the world.",
    popularity: 94.27,
    poster_path: "/lkp1GFmWyf7k2WKvKIQuuGyichI.jpg",
    release_date: "2010-05-19",
    title: "Prince of Persia: The Sands of Time",
    video: false,
    vote_average: 6.2,
    vote_count: 5585,
  },
  {
    adult: false,
    backdrop_path: "/fCsi39CuitAmLSUvUy1EWmKETNx.jpg",
    genre_ids: [27],
    id: 10585,
    original_language: "en",
    original_title: "Child's Play",
    overview:
      "A single mother gives her son a beloved doll for his birthday, only to discover that it is possessed by the soul of a serial killer.",
    popularity: 106.33,
    poster_path: "/433jjDcHFEFa9S1eMgkVaK3W900.jpg",
    release_date: "1988-11-08",
    title: "Child's Play",
    video: false,
    vote_average: 6.6,
    vote_count: 2061,
  },
];

const ProductsList = () => {
  const [visible, setVisible] = useState(false);
  const [info, setInfo] = useState({
    title: "",
    overview: "",
    poster_path: "",
    id: "",
    vote_average: "",
    vote_count: "",
  });

  const onClose = () => {
    setVisible(false);
  };

  return (
    <>
      <List
        itemLayout="vertical"
        grid={{
          gutter: 16,
          xs: 1,
          sm: 2,
          md: 4,
          lg: 4,
          xl: 6,
          xxl: 3,
        }}
        style={{ margin: "24px" }}
        dataSource={data}
        pagination={{
          onChange: (page) => {
            console.log(page);
          },
          pageSize: 6,
        }}
        renderItem={(item) => (
          <List.Item>
            <Card
              hoverable
              style={{ height: 700 }}
              cover={
                <img
                  alt={item.title}
                  src={`https://image.tmdb.org/t/p/w185${item.poster_path}`}
                  style={{ height: 480, objectFit: "cover" }}
                />
              }
              onClick={() => {
                setVisible(true);
                setInfo(item);
              }}
            >
              <Meta title={item.title} description={item.overview} />
            </Card>
          </List.Item>
        )}
      />
      <DrawerProduct
        visible={visible}
        onClose={onClose}
        title={info.title}
        overview={info.overview}
        poster_path={info.poster_path}
        id={info.id}
        vote_average={info.vote_average}
        vote_count={info.vote_count / 100}
      />
    </>
  );
};

export default ProductsList;
