import MoneyInput from 'components/form/money-input'
import createFormikField from 'hoc/formik-field'

const MoneyInputField = createFormikField(MoneyInput)

export default MoneyInputField
