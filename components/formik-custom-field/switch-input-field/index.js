import Switch from 'components/form/switch'
import createFormikField from 'hoc/formik-field'

const SwitchField = createFormikField(Switch)

export default SwitchField
