import Select from 'components/form/select'
import createFormikField from 'hoc/formik-field'

const SelectDropdownField = createFormikField(Select.Dropdown)

export default SelectDropdownField
