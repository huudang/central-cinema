import DatePicker from '../../form/date-picker'
import createFormikField from 'hoc/formik-field'

const DatePickerField = createFormikField(DatePicker)

export default DatePickerField
