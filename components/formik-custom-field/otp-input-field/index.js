import OTPInput from 'components/form/otp-input'
import createFormikField from 'hoc/formik-field'

const OTPInputField = createFormikField(OTPInput)
export default OTPInputField
