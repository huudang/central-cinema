import Radio from 'components/form/radio'
import createFormikField from 'hoc/formik-field'

const RadioField = createFormikField(Radio)

export default RadioField
