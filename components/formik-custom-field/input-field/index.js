import { Input } from 'components/antd'
import createFormikField from '../../../hoc/formik-field'

const InputField = createFormikField(Input)

export default InputField
