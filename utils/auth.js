import cookie, { cookieOptions } from 'utils/cookie'

const AUTH_ID = '@Lightstore/AUTH_ID'
const AUTH_TOKEN = '@Lightstore/AUTH_TOKEN'
const CART_AMOUNT = '@Lightstore/CART_AMOUNT'

const REDIRECT_PATH = '@Lightstore/REDIRECT_PATH'

export function saveAmount (value, res) {
  if (res) {
    res.cookie(CART_AMOUNT, value, cookieOptions)
    return
  }

  cookie.save(CART_AMOUNT, value)
}

export function getAmount (req) {
  return cookie.get(CART_AMOUNT, req)
}

export function saveId (id, res) {
  if (res) {
    res.cookie(AUTH_ID, id, cookieOptions)
    return
  }

  cookie.save(AUTH_ID, id)
}

export function getId (req) {
  return cookie.get(AUTH_ID, req)
}

export function saveToken (token, res) {
  if (res) {
    res.cookie(AUTH_TOKEN, token, cookieOptions)
    return
  }

  cookie.save(AUTH_TOKEN, token)
}

export function getToken (req) {
  return cookie.get(AUTH_TOKEN, req)
}

export function saveRedirectPath (res, url) {
  if (res) {
    res.cookie(REDIRECT_PATH, url, cookieOptions)
    return
  }

  cookie.save(REDIRECT_PATH, url)
}

export function getRedirectPath (req) {
  return cookie.get(REDIRECT_PATH, req)
}

export function logout (to) {
  saveId('')
  saveToken('')
  saveRedirectPath('')

  if (to) {
    window.location = to
  } else {
    window.location = '/auth'
  }
}
