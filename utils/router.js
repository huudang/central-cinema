import { Router } from './I18nUtils'

const RouterUtils = {
  push: (router, ...args) => {
    window.scrollTo(0, 0)

    if (typeof router === 'string') {
      Router.push(router, ...args)
      return
    }

    Router.push(router.href, router.as, ...args)
  },

  replace: (router, ...args) => {
    if (typeof router === 'string') {
      return Router.replace(router, ...args)
    }

    return Router.replace(router.href, router.as, ...args)
  },

  redirect: (context, target) => {
    if (context.res) {
      context.res.writeHead(303, { Location: typeof target === 'string' ? target : (target?.as || target?.href || '/') })
      context.res.end()
    } else {
      // In the browser, we just pretend like this never even happened
      RouterUtils.replace(target)
    }
  }
}

export default RouterUtils
