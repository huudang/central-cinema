import axios from 'axios'
import { getToken } from '../utils/auth'

global.headers = {
  Accept: 'application/json'
}

global.authToken = ''

export const setGlobalAuthToken = (authToken) => {
  global.authToken = authToken
}

export const setGlobalHeaders = (headers) => {
  if (headers) {
    global.headers = {
      ...global.headers,
      ...headers
    }
  }
}

export const getHeaders = () => {
  const currentToken = process.browser ? getToken() : ''
  if (currentToken) {
    setGlobalAuthToken(currentToken)
  }

  if (global.authToken) {
    global.headers = {
      ...global.headers,
      Authorization: 'Bearer ' + global.authToken
    }
  }

  return global.headers
}

const getAttributes = (props = {}) => ({
  cache: true,
  headers: getHeaders(),
  ...props
})

const customAxios = func => (url, ...args) => {
  return new Promise((resolve, reject) => {
    func(url, ...args)
      .then(({ data, status }) => {
        resolve({ data, status, success: true })
      })
      .catch(e => {
        if (e.response) {
          const { data, status } = e.response
          resolve({ data, status, error: true })
        } else {
          reject(e)
        }
      })
  })
}

export default {
  get: (url, args = {}) => {
    return customAxios(axios.get)(url, getAttributes(args))
  },
  post: (url, data, args = {}) => {
    return customAxios(axios.post)(url, data, getAttributes(args))
  },
  put: (url, data, args) => {
    return customAxios(axios.put)(url, data, getAttributes(args))
  },
  delete: (url, args) => {
    return customAxios(axios.delete)(url, getAttributes(args))
  }
}
