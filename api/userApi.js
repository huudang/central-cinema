import { getMainApi } from '../config'
import axios from '../utils/axios'

const c = (path = '') => {
  return getMainApi().auth + path
}

const userApi = {
  getProfile: token => {
    return axios.get(c('/me'))
  }
}

export default userApi
