import { getMainApi } from '../config'
import axios from '../utils/axios'

const c = (path = '') => {
  return getMainApi().auth + path
}

const authApi = {
  login: ({ identifier, password }) =>
    axios.post(c('/local'), {
      identifier,
      password
    })
}

export default authApi
